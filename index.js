`use strict`;

const 
    { service, amqp, mqtt, subscriber, database, router} = require(`./app_module`);

let start = () => {
    return new Promise(async (resolve, reject) => {
        resolve({
            service : await service(),
            amqp    : await amqp(),
            //mqtt    : await mqtt(),
            db      : await database(),
            router  : await router()
        })
     }).then(async (data) => {
        subscriber();
     });
};

start()