`use strict`;
const
authorization = require(`${basedir}/src/middleware/authorization`);

module.exports = (route) => {

    route.get(`/info`,[authorization], (req, res) => {
        control.user.info(req, res)
    });

    route.get(`/role`,[authorization], (req, res) => {
        control.user.role(req, res)
    });

    route.post(`/create`,[authorization], (req, res) => {
        control.user.create(req, res)
    });

    route.get(`/list`,[authorization], (req, res) => {
        control.user.list(req, res)
    });

    return route;
}