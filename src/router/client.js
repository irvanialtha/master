`use strict`;
const
    authorization = require(`${basedir}/src/middleware/authorization`);

    
module.exports = (route) => {
    route.get(`/get`,[authorization], (req, res) => {
        control.client.get(req, res)
    });

    route.get(`/role`,[authorization], (req, res) => {
        control.client.role(req, res)
    });

    route.post(`/register`,[authorization], (req, res) => {
        control.client.register(req, res)
    });

    route.get(`/getbyuser`,[authorization], (req, res) => {
        req.body = {
            userid : req.headers.env.userinfo.user_id
        }
        control.client.get(req, res)
    });

    route.get(`/getfloorclient`,[authorization], (req, res) => {
        control.client.getfloorclient(req, res)
    });

    route.get(`/getfloordevice`,[authorization], (req, res) => {
        control.client.getfloordevice(req, res)
    });

    route.post(`/createdevice`,[authorization], (req, res) => {
        control.client.createdevice(req, res)
    });

    route.get(`/typedevice`,[authorization], (req, res) => {
        control.client.typedevice(req, res)
    });

    route.get(`/typedevice`,[authorization], (req, res) => {
        control.client.typedevice(req, res)
    });

    route.get(`/device`,[authorization], (req, res) => {
        control.client.getdevice(req, res)
    });
    
    route.get(`/getappliancefloor`,[authorization], (req, res) => {
        control.client.getappliancefloor(req, res)
    });

    route.get(`/getalldevice`,[authorization], (req, res) => {
        control.client.getalldevice(req, res)
    });
    
    route.get(`/getchangeincost`,[authorization], (req, res) => {
        control.client.getchangeincost(req, res)
    });

    route.get(`/getusageperdevice`,[authorization], (req, res) => {
        control.client.getusageperdevice(req, res)
    });

    route.get(`/getusageperfloor`,[authorization], (req, res) => {
        control.client.getusageperfloor(req, res)
    });
    route.get(`/downloadreportdevice`,[authorization], (req, res) => {
        control.client.downloadreportdevice(req, res)
    });
    route.get(`/downloadreportdevice2`,[authorization], (req, res) => {
        control.client.downloadreportdevice2(req, res)
    });

    route.post(`/getdevicestatus`,[authorization], (req, res) => {
        control.client.getdevicestatus(req, res)
    });

    return route;
}