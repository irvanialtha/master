`use strict`;
const
    authorization = require(`${basedir}/src/middleware/authorization`);
    
module.exports = (route) => {

    route.get(`/`, (req, res) => {
        res.send('success')
    });

    route.post(`/authenticate`, (req, res) => {
        control.auth.authenticate(req, res)
    });

    route.post(`/logout`, (req, res) => {
        control.auth.logout(req, res)
    });

    return route;
}