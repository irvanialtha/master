`use strict`;

module.exports = {
    
    authenticate: (req, res) => {
        amqp.publish(service.auth, 'authentication.authenticate', {...req });
    },

    logout: (req, res) => {
        amqp.publish(service.user, 'authentication.logout', {...req });
    },
};