`use strict`;

module.exports = {
    get: (req, res) => {
        amqp.publish(service.client, 'client.get', {...req });
    },
    role: (req, res) => {
        amqp.publish(service.client, 'client.role', {...req });
    },
    register: (req, res) => {
        amqp.publish(service.client, 'client.register', {...req });
    },
    getfloorclient: (req, res) => {
        req.params = req.query;
        amqp.publish(service.client, 'client.getfloorclient', {...req });
    },
    getfloordevice: (req, res) => {
        req.params = req.query;
        amqp.publish(service.client, 'client.getfloordevice', {...req });
    },
    createdevice: (req, res) => {
        req.params = req.query;
        amqp.publish(service.client, 'client.createdevice', {...req });
    },
    typedevice: (req, res) => {
        req.params = req.query;
        amqp.publish(service.client, 'client.typedevice', {...req });
    },
    getdevice: (req, res) => {
        req.params = req.query;
        amqp.publish(service.client, 'client.getdevice', {...req });
    },  
    getappliancefloor: (req, res) => {
        req.params = req.query;
        amqp.publish(service.client, 'client.getappliancefloor', {...req });
    },  
    getalldevice: (req, res) => {
        req.params = req.query;
        amqp.publish(service.client, 'client.getalldevice', {...req });
    },  
    getchangeincost: (req, res) => {
        req.params = req.query;
        amqp.publish(service.client, 'client.getchangeincost', {...req });
    },  
    getusageperdevice: (req, res) => {
        req.params = req.query;
        amqp.publish(service.client, 'client.getusageperdevice', {...req });
    },  
    getusageperfloor: (req, res) => {
        req.params = req.query;
        amqp.publish(service.client, 'client.getusageperfloor', {...req });
    },  

    downloadreportdevice: (req, res) => {
        req.params = req.query;
        amqp.publish(service.client, 'client.downloadreportdevice', {...req });
    },

    downloadreportdevice2: (req, res) => {
        req.params = req.query;
        amqp.publish(service.client, 'client.downloadreportdevice2', {...req });
    },

    getdevicestatus: async (req, res) => {
        const result = await model.main.getdevicestatus(req.body);
        res.send(result);
    },   
};