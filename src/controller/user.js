`use strict`;

module.exports = {

    info: (req, res) => {
        amqp.publish(service.user, 'user.info', {...req });
    },

    role: (req, res) => {
        amqp.publish(service.user, 'user.role', {...req });
    },

    create: (req, res) => {
        amqp.publish(service.user, 'user.create', {...req });
    },

    list: (req, res) => {
        amqp.publish(service.user, 'user.list', {...req });
    }

};