`use strict`;
var fs      = require('fs'),
    Excel   = require('exceljs'),
    moment  = require('moment');

module.exports = {
    output:(req, res, data)=>{
        res.send(data.body)
    },
    exportdevice:(req, res, data)=>{
        console.log(data)
        // create workbook & add worksheet
        let workbook    = new Excel.Workbook()
            worksheet   = workbook.addWorksheet('Data'),
            sheet       = data.body.data.map(object =>{
                return {
                    ... object,
                    device_detail : data.params.device_detail[object.device_name].description
                }
            });
            //sheet       = data.body.data;

        for (const i in sheet) {
            if (i == 0) {
                let columns         = sheet[i],
                    columnsSheet    = [{
                        header  : 'time',
                        key     : 'time'
                    }];
                for (const col in columns ) {
                    if (col != 'time') {
                        columnsSheet.push({
                            header  : col,
                            key     : col
                        })
                    }
                }
                if (sheet[i].temperature) {
                    columnsSheet.push({
                        header  : 'temp_status',
                        key     : 'temp_status'
                    })
                }
                if (sheet[i].humidity) {
                    columnsSheet.push({
                        header  : 'hum_status',
                        key     : 'hum_status'
                    })
                }
                if (sheet[i].voltage) {
                    columnsSheet.push({
                        header  : 'vol_status',
                        key     : 'vol_status'
                    })
                }
                worksheet.columns = columnsSheet;
            }
            let 
                rows        = sheet[i];
                rows.time   = moment(sheet[i].time).format('DD.MM.YYYY hh:mm:ss');
                if (sheet[i].temperature) {
                    rows.temp_status = parseFloat(sheet[i].temperature) >= 21 && parseFloat(sheet[i].temperature) <= 25 ? 'Safe' : 'Unsafe'
                }
                if (sheet[i].humidity) {
                    rows.hum_status = parseFloat(sheet[i].humidity) >= 40 && parseFloat(sheet[i].humidity) <= 70 ? 'Safe' : 'Unsafe'
                }
                if (sheet[i].voltage) {
                    rows.vol_status = parseFloat(sheet[i].voltage) >= 209 && parseFloat(sheet[i].voltage) <= 231 ? 'Safe' : 'Unsafe'
                }
            worksheet.addRow(rows);
        }

        // save workbook to disk
        let 
            time = new Date().getTime();
            file = `${basedir}/protected/export_${time}.xlsx`;

        workbook.xlsx.writeFile(file)
        .then(function() {
            let exceldata = fs.readFileSync(file,(err, data) => {
                if (err) {
                    console.error('error', err);
                }
                return data
            });
            return exceldata;
        })
        .then(function(exceldata) {
            res.setHeader('Content-Length', fs.statSync(file).size);
            res.setHeader('Content-Disposition', `attachment; filename="export.xlsx"`)
            res.send(exceldata);
        })
        .then(function(exceldata) {
            fs.unlink(file,(error) =>{
                if (error) {
                    console.log(error)
                }
            })
        });   
    }
}