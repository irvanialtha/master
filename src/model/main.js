`use strict`;

module.exports = {
    getdevicestatus: (data) => {
        return new Promise((resolve) => {
            Promise.all(data.device.map(async (item) => {
                const device = await db.metrics.findAll({
                    order: [
                        ['time', 'DESC']
                    ],
                    attributes: [
                        'device_name', 'data', 'time', 'client_id'
                    ],
                    where : {
                        device_name : item.deviceid
                    },
                    limit: 1,
                    raw: true
                });
                if (device.length) {
                    return [{
                        ... device[0],
                        description: item.description,
                        devicename: item.devicename
                    }]
                } else {
                    return [];
                }
            })).then(async (raw) => {
                const result = raw.flat();
                const active_device = await module.exports.filterActiveDevice(result);
                const status_device = await module.exports.filterStatusDevice(result);
                resolve({
                    active_device: active_device,
                    status_device: status_device
                });
            });
        });
    },
    filterActiveDevice: (data) => {
        const today = new Date();
        return new Promise((resolve) => {
            const result = data.map((item) => {
                const device_time = new Date(item.time);
                if (today.getTime() - device_time.getTime() <= 1000*3600*6) {
                    return {
                        deviceid: item.device_name,
                        time: item.time,
                        description: item.description,
                        devicename: item.devicename,
                        isActive: true
                    }
                } else {
                    return {
                        deviceid: item.device_name,
                        time: item.time,
                        description: item.description,
                        devicename: item.devicename,
                        isActive: false
                    }
                }
            });
            resolve(result);
        });
    },
    filterStatusDevice: (data) => {
        return new Promise((resolve) => {
            const result = data.reduce((acc, val) => {
                const properties = val.data.properties;

                if (properties.modbus == 0) {
                    acc.push({
                        deviceid: val.device_name,
                        time: val.time,
                        description: val.description,
                        devicename: val.devicename,
                        status: 'warning',
                        message: 'Modbus device failed'
                    });
                }

                if (properties.sensor == 0) {
                    acc.push({
                        deviceid: val.device_name,
                        time: val.time,
                        description: val.description,
                        devicename: val.devicename,
                        status: 'warning',
                        message: 'Sensor device failed'
                    });
                }
                
                return acc;
            }, []);
            resolve(result);
        });
    }
}