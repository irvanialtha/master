'use strict';
const
    middleware = (req, res, next) =>{

        const 
            requestId = req.requestid;
            req.next  = next

        if (requestId) {
            amqp.publish(service.auth,'authorization.authorize',{... req});
        } else {
            res.status(403).end();
        }
    };

    middleware.next = (req, res, next, data) =>{
        if (data.body.valid) { 
            req.headers.env = data.body.env
            next();
        } else {
            res.status(403).end();
        }
    }

module.exports = middleware
